# README #

Install python3 on your system if you don't already have it debian/base(sudo apt-get install python3.4)
At the terminal you can execute the script with python3 main.py after cd'ing into the directory where you cloned
you may also choose to chmod +x main.py so that you can execute the script directly by typing main into the terminal
if your paths have been setup.

### What is this repository for? ###

* Presents user with a GUI that alows easy tweaking of the xinput properties controlling mouse acceleration for
Linux users with high dpi / gaming mice who find the current settings way too fast.  Creates and saves a script file
you may place into your xinit or startup session applications list to keep settings upon reboot.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install python3, cd to the directory you cloned to , run python3 main.py
* Only optional configuration is to chmod +x the scripts (main.py and script.sh) and to add script.sh to startup
* No dependancies

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact