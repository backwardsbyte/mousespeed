"""
This script provides Linux users with high dpi/gaming mice a easy way to set properties used
to controll mouse acceleration and speed.  Because such mice move too fast in a default Linux
install and the settings manager does not provide a way to change this.

You get a simple GUI interface with a slider and a spinbox to allow you to easily change these
settings.  It generates a script which you can chmod +x to make it executable and place in your
startup applications via your own method i.e. shell sessions, xinit

The idea is not new, I found a way to manualy do it at Patrick Nielsen's blog:
http://patrickmylund.com/blog/lowering-gaming-mouse-sensitivity-in-ubuntu-9-10/

Others have made shell scripts to do this for you.  I am learning python and made this program
for both myself and to share and learn from.
"""

#import various modules used, subprocess to execute shell command xinput interacts with OS
from subprocess import check_output, call
from tkinter import *
from tkinter import ttk
import re

#consider these constants
#regex patterns for extracting data from script and xinput
DECELERATION = re.compile('Deceleration.*([\d]+)')
VELOCITY = re.compile('Scaling.*([\d]+)')
POINTER = re.compile(r'([\s\w\d]*Mouse[\s\w\d]+)(?=id.*slave\s+pointer)',re.IGNORECASE)
#filename
SCRIPT = 'script.sh'

class Application(Frame):
    """Main application window"""
    def __init__(self, master):
        """Initialize the frame, grab current script to BUFFER, read values, create widgets"""
        Frame.__init__(self, master)
        
        self.file = open(SCRIPT, 'r+')
        self.BUFFER = self.file.read()
        self.file.close()
        
        self.settings = self.getSettingsCurrent()
        self.createWidgets()
        master.title(self.settings['pointer'].split(':')[1]) #extract device name
        master.geometry('300x150')
        master.protocol('WM_DELETE_WINDOW', self.onExit) #use this to update script file
    
    def createWidgets(self):
        """Creates widgets and variables for the GUI and initializes layout onto grid"""
        #some variables used between widgets, and their default values loaded from script
        self.velocity = StringVar()
        self.deceleration = StringVar()        
        self.velocity.set(int(self.settings['velocity']))
        self.deceleration.set(int(self.settings['deceleration']))
        
        #descriptive labels and a button to test settings, labels are static
        Label(self, text='Velocity').grid(row=0, padx=10, sticky=E)
        Label(self, text='Deceleration').grid(row=1, padx=10, sticky=E)
        self.runBtn = Button(self, text='Run', command=self.onRun)
        
        #widgets for user to tweak values, attach to tkinter string variables
        #for tracing and syncronization between widgets
        self.velocityScale = ttk.Scale(self, orient=HORIZONTAL, from_=1, to=15, variable=self.velocity)
        self.decelerationScale = ttk.Scale(self, orient=HORIZONTAL, from_=1, to=15, variable=self.deceleration)
        self.velocitySpinbox = Spinbox(self, textvariable=self.velocity, width=4)
        self.decelerationSpinbox = Spinbox(self, textvariable=self.deceleration, width=4)
      
        #onChanged will keep the values as whole numbers, rounding off
        self.velocity.trace('w', self.onChanged)
        self.deceleration.trace('w', self.onChanged)
        
        #layout
        self.velocityScale.grid(row=0, column=1, padx=10, pady=20, sticky=W)
        self.decelerationScale.grid(row=1, column=1, padx=10, pady=20, sticky=W)
        self.velocitySpinbox.grid(row=0, column=2, padx=20, sticky=W)
        self.decelerationSpinbox.grid(row=1, column=2, padx=20, sticky=W)
        self.runBtn.grid(row=2, column=0, columnspan=3)
        
        #pack frame into root
        self.pack()
        
    def onChanged(self, name, index, mode):
        """Called when one of the two scales change value, keeps values whole integers"""
        value = self.master.globalgetvar(name)
        self.master.globalsetvar(name, round(value))
        
    def onRun(self):
        """uses xinput to set current values for testing, user can determine if speed is good or not"""
        properties = {'"Device Accel Constant Deceleration"': self.deceleration.get(), \
                      '"Device Accel Velocity Scaling"': self.velocity.get()}
        
        command = 'xinput --set-prop'
        
        for prop in properties:
            call(command + ' "' + self.settings['pointer'] + '" '+ prop + ' ' + properties[prop], shell=True)
                  
    def onExit(self):
        """Called when window is closed, saves changes to new script file"""
        newscript = '#!/bin/sh\nNAME="' + self.settings['pointer'] + '"' + \
            '\nxinput --set-prop $NAME "Device Accel Constant Deceleration" ' + \
            self.deceleration.get() + \
            '\nxinput --set-prop $NAME "Device Accel Velocity Scaling" ' + \
            self.velocity.get()
       
        file = open(SCRIPT, 'w')
        file.writelines(newscript)
        file.close()
        
        #apply current settings and quit
        self.onRun()
        self.master.quit()
        
    def getSettingsCurrent(self):    
        """get pointer name, velocity, deceleration settings from BUFFER"""
        #prefix because my mouse showed up twise in xinput --list
        #nothing after prefix indicates fresh script file (first run)
        pointer = re.findall(r'pointer:.+(?=")', self.BUFFER)
        if len(pointer) == 0: pointer = self.getDevice()
        else: pointer = pointer[0]
        velocity = re.findall(VELOCITY, self.BUFFER)[0]
        deceleration = re.findall(DECELERATION, self.BUFFER)[0]
        return {'pointer':pointer, 'velocity':velocity, 'deceleration':deceleration}
    
    def getDevice(self):
        """returns string representing the mouse device used for xinput"""
        output = check_output('xinput --list --short', shell=True).decode('utf-8')    
        
        for line in output.splitlines():
            result = re.search(POINTER, line)
            if result:
                result = result.group(0).strip()
                return 'pointer:' + result #prefix because my mouse shows up twise in xinput --list    

if __name__ == '__main__':
    """Create our root frame, build application and run main loop for the GUI"""
    root = Tk()
    app = Application(root)
    root.mainloop()

